CXX = g++

INCLUDES = -I include \
           -I /usr/include/python3.7

SRC = src/config_loader/configloader.cpp \
      src/move/movement.cpp \
      src/move/ui_data_handler.cpp \
      src/common/py_handler.cpp \
      App/MainApp.cpp

LIB_PATH = -L /usr/lib/python3.7/config-3.7m-arm-linux-gnueabihf

ARTIFACTS = /home/pi/Desktop/artifacts


$(ARTIFACTS)/AppMain: $(SRC)
	$(CXX) $(SRC) -o $(ARTIFACTS)/AppMain $(INCLUDES) -std=c++17  $(LIB_PATH) -lwiringPi -pthread -lboost_system -lpython3.7 

clean:
	rm -fr $(ARTIFACTS)/*
