#ifndef CONFIG_LOADER
#define CONFIG_LOADER
#include <iostream>
#include <vector>
#include <unordered_map>
#include <string>
#include <fstream>
#include <wiringPi.h>

#define CFG_FILE "/var/cfg/kutti.cfg"

using namespace std;


#define INPUT_PIN INPUT
#define OUTPUT_PIN OUTPUT

class Config_Loader {
	private :
		Config_Loader();

		void mreadCfg();
		
		static Config_Loader* mspConfigInstance ;
		unordered_map <string, int> mCfgMap;

		
	public :
		static Config_Loader* getConfigInstance();
		void getMyCfg(unordered_map<string, int> &);
		void getMyCfg(unordered_map<string, pair<int,int>> &);
	
};




#endif
