#ifndef UI_DATA_HANDLER
#define UI_DATA_HANDLER
#include <boost/asio.hpp> 
#include <boost/bind.hpp>

#include <iostream> 
#include <thread>
#include <mutex>
#define MAX_PKT_SIZE 1500


typedef std::shared_ptr<std::vector<char>> shared_buffer ;

class UIDataHandler    
{
     public :
		UIDataHandler(boost::asio::io_service &service, int port);
		void startRead();
     protected:
         void readHandle(const boost::system::error_code& error, size_t bytes, shared_buffer buffer);
         
         
         std::mutex my_lock;
         std::vector<std::string> myDataBuffer ;
         boost::asio::ip::udp::socket mySocket ;
         std::vector< shared_buffer > readBuffer ;
         boost::asio::io_service &myService;

};

#endif
