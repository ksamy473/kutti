#ifndef MOVEMENT
#define MOVEMENT
#include "config.h"
#include "ui_data_handler.h"

#include <unistd.h>

#define MOTOR_FORWARD 1
#define MOTOR_REVERSE 0
#define MOTOR_STOP -1

#define ON_THE_WAY 1
#define NO_LINE -1
#define REACHED_STATION 0



/**************************************
 ************************************** 
 ** DESCRIPTION : Movement modules
 ** 
 **
 **
 **************************************
 **************************************/
class Movement : public UIDataHandler
{
	friend class Config_Loader;
	
	public :
		Movement(boost::asio::io_service &service, int port);
		void updateMyConfig() ;
		void startMoveTh();

	private :
	
		unordered_map <string, pair<int,int>> mmovePin;
		bool mkeepMove;
		

		int    mfollowLine();
		void   msetMovePin();
		void   mwheelRotation(int dirLWheel, int dirRWheal);
		void   mmoveControl();
		double mgetDistance();
		
		
		
};

#endif
