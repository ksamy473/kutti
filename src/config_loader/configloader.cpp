
#include <algorithm>
#include "config.h"

Config_Loader*
Config_Loader::mspConfigInstance = NULL;

Config_Loader::Config_Loader()
{
	wiringPiSetupPhys();
	cout << "Config_Loader created " << endl;
}


Config_Loader* 
Config_Loader::getConfigInstance() 
{
		if (Config_Loader::mspConfigInstance == NULL )
		{
			Config_Loader::mspConfigInstance = new Config_Loader();
			Config_Loader::mspConfigInstance->mreadCfg();
		}
		return Config_Loader::mspConfigInstance;
}

void
Config_Loader::mreadCfg()
{
	ifstream cfgfile(CFG_FILE);
	string CfgVar, Eql;
	int CfgVal;
	vector<int> myPins;
	while (!cfgfile.eof())
	{
		cfgfile >> CfgVar;
		cfgfile >> Eql;
		cfgfile >> CfgVal;
	
		if ( Eql == "=" && find(myPins.begin(),myPins.end(), CfgVal) == myPins.end())
		{
			mCfgMap[CfgVar] = CfgVal ;
			cout << CfgVar << " --> " << mCfgMap[CfgVar] << endl;
		}
	}
}
void
Config_Loader::getMyCfg(unordered_map<string, int> & Map)
{
	for ( auto Mp : Map )
	{
		if (mCfgMap.find(Mp.first) != mCfgMap.end() )
		{
			Map[Mp.first] = mCfgMap[Mp.first];
		}
			cout << Mp.first <<" " << Map[Mp.first]  << endl;
	}
}

void
Config_Loader::getMyCfg(unordered_map<string, pair<int,int>> & Map)
{
	for ( auto Mp : Map )
	{
		if (mCfgMap.find(Mp.first) != mCfgMap.end() )
		{

			Map[Mp.first].first = mCfgMap[Mp.first];
		}
			cout << Mp.first <<" " << Map[Mp.first].first <<" "<<Map[Mp.first].second  << endl;
	}
}
