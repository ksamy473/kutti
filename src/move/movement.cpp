#include "move.h"
#include <chrono>
#include <ratio>

extern string loadPy(vector<string> &argv);
Movement::Movement(boost::asio::io_service &service, int port) : UIDataHandler(service ,port)
{
	      //pair<pinname, pintype> -- 0 for input,  1 for output
	vector<pair<string, int>> Pins = { {"LEFT_IR" ,INPUT_PIN },
									   {"RIGHT_IR",INPUT_PIN },
									   {"CENTER_IR",INPUT_PIN },
									   {"LEFT_MOTOR_FRW", OUTPUT_PIN},
									   {"LEFT_MOTOR_RVS", OUTPUT_PIN},
									   {"RIGHT_MOTOR_FRW",OUTPUT_PIN},
									   {"RIGHT_MOTOR_RVS",OUTPUT_PIN},
									   {"ULTRA_TRIG",OUTPUT_PIN},
									   {"ULTRA_ECHO",INPUT_PIN}
		                   } ;
	for ( auto Pn : Pins )
	{
		mmovePin[Pn.first] = {-1,Pn.second};
	}
	mkeepMove = false;
}

void 
Movement::updateMyConfig() 
{
	Config_Loader * inst = Config_Loader::getConfigInstance();
	inst->getMyCfg(mmovePin);
	msetMovePin();
}

void
Movement::msetMovePin()
{
	for ( auto pin : mmovePin )
	{
		pinMode(pin.second.first, pin.second.second);
	}

}

void
Movement::startMoveTh()
{
	mkeepMove = true;

	std::thread readTh(&UIDataHandler::startRead, this);
	std::thread cntlTh(&Movement::mmoveControl, this);

	readTh.join();
	cntlTh.join();
/*	while(true)
	{
		mfollowLine();
		usleep(1);
	}
*/
	//mgetDistance();
}

void
Movement::mmoveControl()
{
	while (true)
	{
		std::vector<std::string> tmpStrBuffer;
		my_lock.lock();
		  tmpStrBuffer = std::move(myDataBuffer);
		my_lock.unlock();
		for (auto str : tmpStrBuffer)
			std::cout << "Received Instruction : " <<str<< endl;

		usleep(100000);
	}

}

int
Movement::mfollowLine()
{
	int leftIR = digitalRead(mmovePin["LEFT_IR"].first);
	int rightIR = digitalRead(mmovePin["RIGHT_IR"].first);
	int centerIR = digitalRead(mmovePin["CENTER_IR"].first);

	int dirLWheel = MOTOR_STOP ;
	int dirRWheal = MOTOR_STOP ;
	int status = ON_THE_WAY;

	if(leftIR || centerIR || rightIR)
	{
		if ( ! leftIR && centerIR && ! rightIR)
		{
			dirLWheel = MOTOR_FORWARD ;
			dirRWheal = MOTOR_FORWARD ;
		}
		else if ( (! leftIR && centerIR &&  rightIR ) or (! leftIR && ! centerIR &&  rightIR ))
		{
			dirLWheel = MOTOR_FORWARD ;
		}
		else if ( ( leftIR && centerIR &&  ! rightIR ) or ( leftIR && ! centerIR && ! rightIR ))
		{
			dirRWheal = MOTOR_FORWARD ;
		}
		else
		{
			status = REACHED_STATION ;
		}
	}
	else
	{
		status = NO_LINE ;
	}

	mwheelRotation(dirLWheel, dirRWheal);
	return status;
}

void
Movement::mwheelRotation(int dirLWheel, int dirRWheal)
{
	int lwPinFrw = LOW ;
	int lwPinRvs = LOW ;
	int rwPinFrw = LOW ;
	int rwPinRvs = LOW ;

	if ( dirLWheel == MOTOR_FORWARD)
	{
		lwPinFrw = HIGH ;
	}
	else if ( dirLWheel == MOTOR_REVERSE )
	{
		lwPinRvs = HIGH ;
	}

	if ( dirRWheal == MOTOR_FORWARD )
	{
		rwPinFrw  = HIGH ;
	}
	else if ( dirRWheal == MOTOR_REVERSE )
	{
		rwPinRvs = HIGH ;
	}

	if (mkeepMove)
	{
		cout <<"LmF = " << lwPinFrw << "LmR =" << lwPinRvs << " RmF = " << rwPinFrw << " RmR = " <<  rwPinRvs << endl;
	}

}

double
Movement::mgetDistance()
{
	cout << "Entered into mgetDistance " << endl;
	std::chrono::time_point<std::chrono::system_clock> start, end;
	int echoPin = mmovePin["ULTRA_ECHO"].first;

	digitalWrite (mmovePin["ULTRA_TRIG"].first, HIGH) ;
	usleep(10); //sleep for 10us
	digitalWrite (mmovePin["ULTRA_TRIG"].first, LOW) ;


	while (digitalRead(echoPin) == 0)
	{
		start = std::chrono::system_clock::now();
	}
	while (digitalRead(echoPin) == 1)
	{
		end = std::chrono::system_clock::now();
	}
	std::chrono::duration<double,std::milli> elapsed_ms = (end - start);

	cout << elapsed_ms.count()<<endl;
	double distance = (elapsed_ms.count()* 34300) / (1000*2);
	cout << distance << endl;
	return distance;
}
