#include "ui_data_handler.h"


UIDataHandler::UIDataHandler(boost::asio::io_service &service ,int port) : myService(service), mySocket(service)
{
std::cout << "Object created start" <<std::endl;
  boost::asio::ip::udp::endpoint lstn_endpoint(boost::asio::ip::udp::v4(),port);
  mySocket.open(lstn_endpoint.protocol());
  mySocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
  mySocket.bind(lstn_endpoint);
  shared_buffer rcv_buff(new std::vector<char>(MAX_PKT_SIZE));
  
  mySocket.async_receive(boost::asio::buffer(*rcv_buff), boost::bind(&UIDataHandler::readHandle, this, 
                                                                      boost::asio::placeholders::error,
								      boost::asio::placeholders::bytes_transferred,
								      rcv_buff)
								      );
    std::cout << "Object created end" <<std::endl;
  
}


void 
UIDataHandler::readHandle(const boost::system::error_code& error, size_t bytes, shared_buffer buffer)
{
    std::cout << "Received some data" <<std::endl;
  
   shared_buffer rcv_buff(new std::vector<char>(MAX_PKT_SIZE));

   std::string data(std::begin(*buffer),std::begin(*buffer)+bytes);

   my_lock.lock();
      myDataBuffer.push_back(data);
      std::cout << data << std::endl;
   my_lock.unlock();
   
   mySocket.async_receive(boost::asio::buffer(*rcv_buff), boost::bind(&UIDataHandler::readHandle, this, 
                                                                      boost::asio::placeholders::error,
								      boost::asio::placeholders::bytes_transferred,
								      rcv_buff)
								      );
}


void 
UIDataHandler::startRead()
{
  std::cout << " starting service " << std::endl;
  myService.run();

}
