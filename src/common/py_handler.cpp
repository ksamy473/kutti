#include <iostream>
#include <Python.h>
#include <string>
#include <vector>
#include <stdio.h>
#include <algorithm>

std::string loadPy(std::vector<std::string> &argv)
{
    PyObject *pName, *pModule, *pFunc;
    PyObject *pArgs, *pValue;
    int i;
    
    std::string result("Error");
	int input_size = argv.size();
    if ( input_size < 2) {
        fprintf(stderr,"Usage: call pythonfile funcname [args]\n");
        return result;
    }
	const char *mdlName = argv[0].c_str();
	const char *fnName = argv[1].c_str();
    Py_Initialize();

    pName = PyBytes_FromString(mdlName);
    /* Error checking of pName left out */

    pModule = PyImport_ImportModule(mdlName);

    Py_DECREF(pName);

    if (pModule != NULL) {
        pFunc = PyObject_GetAttrString(pModule, fnName);
        /* pFunc is a new reference */

        if (pFunc && PyCallable_Check(pFunc)) {
            pArgs = PyTuple_New(input_size - 2);
            for (i = 0; i < input_size - 2; ++i) {
                pValue = PyLong_FromLong(atoi(argv[i + 2].c_str()));
                if (!pValue) {
                    Py_DECREF(pArgs);
                    Py_DECREF(pModule);
                    fprintf(stderr, "Cannot convert argument\n");
                    return result;
                }
                /* pValue reference stolen here: */
                PyTuple_SetItem(pArgs, i, pValue);
            }
            pValue = PyObject_CallObject(pFunc, pArgs);
            Py_DECREF(pArgs);
            if (pValue != NULL) {
				result = PyBytes_AsString(pValue);
                Py_DECREF(pValue);
                
            }
            else {
                Py_DECREF(pFunc);
                Py_DECREF(pModule);
                PyErr_Print();
                fprintf(stderr,"Call failed\n");
                return result;
            }
        }
        else {
            if (PyErr_Occurred())
                PyErr_Print();
            fprintf(stderr, "Cannot find function \"%s\"\n", fnName);
        }
        Py_XDECREF(pFunc);
        Py_DECREF(pModule);
    }
    else {
        PyErr_Print();
        fprintf(stderr, "Failed to load \"%s\"\n", mdlName);
        return result;
    }
    Py_Finalize();
    return result;
}
