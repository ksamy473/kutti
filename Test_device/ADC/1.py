import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn
import time
i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)

while True :
    chan0 = AnalogIn(ads, ADS.P0)
    chan1 = AnalogIn(ads, ADS.P1)
    volt = chan0.voltage
    val = int(45+ (volt*20)-((10-(1/(0.1+volt)))*0.15))
    print(val)
    time.sleep(0.04)