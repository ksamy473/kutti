// Server-side Synchronous Chatting Application 
// using C++ boost::asio 

#include <boost/asio.hpp> 
#include <boost/bind.hpp>
#include <string>
#include <iostream> 
#include <thread>
#define MAX_PKT_SIZE 1500


typedef std::shared_ptr<std::vector<char>> shared_buffer ;

class MySock 
{
  public :
    MySock(boost::asio::io_service &service, int port);
    
    void readHandle(const boost::system::error_code& error, size_t bytes, shared_buffer buffer);
    void  sampleTh();
    boost::asio::ip::udp::socket mySocket ;
    std::vector< shared_buffer > readBuffer ;
    boost::asio::io_service &mservice;

};
MySock::MySock(boost::asio::io_service &service ,int port) : mservice(service), mySocket(service)
{
std::cout << "Object created start" <<std::endl;
  boost::asio::ip::udp::endpoint lstn_endpoint(boost::asio::ip::udp::v4(),port);
  mySocket.open(lstn_endpoint.protocol());
  mySocket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
  mySocket.bind(lstn_endpoint);
  shared_buffer rcv_buff(new std::vector<char>(MAX_PKT_SIZE));
  
  mySocket.async_receive(boost::asio::buffer(*rcv_buff), boost::bind(&MySock::readHandle, this, 
                                                                      boost::asio::placeholders::error,
								      boost::asio::placeholders::bytes_transferred,
								      rcv_buff)
								      );
    std::cout << "Object created end" <<std::endl;
  
}
void 
MySock::readHandle(const boost::system::error_code& error, size_t bytes, shared_buffer buffer)
{
  std::cout << "Received some data" <<std::endl;
  
  shared_buffer rcv_buff(new std::vector<char>(MAX_PKT_SIZE));
  std::string data(std::begin(*buffer),std::begin(*buffer)+bytes);

  std::cout << data << std::endl;
  mySocket.async_receive(boost::asio::buffer(*rcv_buff), boost::bind(&MySock::readHandle, this, 
                                                                      boost::asio::placeholders::error,
								      boost::asio::placeholders::bytes_transferred,
								      rcv_buff)
								      );
}

void 
MySock::sampleTh()
{
  std::cout << " starting service " << std::endl;
  mservice.run();

}
int main()
{
  boost::asio::io_service service;
  MySock *mysc = new MySock(service, 50000);
  std::thread t1(&MySock::sampleTh, mysc);
  std::cout << "Thread started " << std::endl;

  t1.join();
  
}
