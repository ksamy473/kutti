#!/bin/bash

LOG_FILE="/home/pi/Desktop/Robot/log/ip_log"

echo "<<=========Enter IP config File ========>>" >> ${LOG_FILE}


IP_conf="/media/usb/IPCONFIG/ip.conf"
GETIP="/media/usb/IPCONFIG/getip"
IP_store="/media/usb/IPCONFIG/IP_addr"

Actl_WPA_conf="/etc/wpa_supplicant/wpa_supplicant.conf"
bk_WPA_conf="/home/pi/backup/wpa_supplicant.conf"

Actl_Interf="/etc/network/interfaces"

USB_connected=$(ls /media/usb | wc -c)

function IP_config() {
	echo "Entering to IP_config" >> ${LOG_FILE}
	if [ -f ${IP_conf} ] ; then 
		echo "enter ip.conf flag found in USB" >> ${LOG_FILE}
		user_name=$( cat ${IP_conf} | awk -F ':' '{print $1}')
		password=$( cat ${IP_conf} | awk -F ':' '{print $2}')
		sudo cp ${bk_WPA_conf} ${Actl_WPA_conf}
		sudo wpa_passphrase "${user_name}" "${password}" | sudo tee -a   ${Actl_WPA_conf} 
		echo "Password stored in wifi config" >> ${LOG_FILE}
		sudo rm -f ${IP_conf} 
		sudo echo "Get ip address" > ${GETIP}
                sudo umount /media/usb
		sleep 60
		espeak -s 100 "please wait, Trying to connect to network"
		sudo dhclient wlan0
		sleep 50
		IP_details=$( hostname -I | sed "s/\./ point /g" )
		espeak -s 100 "${IP_details}"
	elif [ -f ${GETIP} ] ; then 
		echo "GETIP flag found in USB" >> ${LOG_FILE}
		sleep 59
		IP_details="$( ifconfig wlan0 | grep 'inet addr' )"
		sleep 1
		sudo echo "${IP_details}" > /tmp/ipconf
		echo "${IP_details}"  >> ${LOG_FILE}
		sleep 1
		sudo chmod 777 ${IP_store}
		sudo cp /tmp/ipconf ${IP_store}
		if [ ! "$IP_details" == "" ] ;  then	
			sudo rm -f ${GETIP}

		fi
	fi
	echo "Exit to IP_config" >> ${LOG_FILE}

}

Temp_ucnt=0
echo "Check already connected USB"
while [[ ($USB_connected == 0 && $(df -h | grep /media/usb | wc -c ) -gt 0 ) ]] ; do
	
	Temp_ucnt=$( expr ${Temp_ucnt} + 1 )
	umount /media/usb
	echo "count ${Temp_ucnt} Un mounting /media/usb" >> ${LOG_FILE}

done

if [ $USB_connected -gt 0 ] ; then
	echo "USB already connected" >> ${LOG_FILE}
	IP_config
	exit
fi


USB=$(fdisk -l | awk -F ' ' '{print $1}' | grep /dev/sd)
if [ "${USB}" != ""  ] ; then
	mount ${USB} /media/usb 
	echo "USB found" >> ${LOG_FILE}
	IP_config
	
else
	echo "USB not found" >> ${LOG_FILE}
fi
