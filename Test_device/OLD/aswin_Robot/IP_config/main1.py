from thread import start_new_thread
import threading
import servo
import time,subprocess
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(17,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(19,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(22,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(27,GPIO.IN,pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(18,GPIO.IN)

rms=8
lms=6
rel=3
lel=5
rsh=2
lsh=4

def controll(channel,degree1,degree2,delay=0.03):
   servo.set_servo(channel,degree1,degree2,delay)
def speech(self,data,delay=0):
   time.sleep(delay);
   subprocess.Popen("sudo espeak "+str(data),shell=True)
#subprocess.Popen("sudo python face1.py ",shell=True)
def reset():
   t1 = threading.Thread(target=controll,args=(2,90,90))
   t2 = threading.Thread(target=controll,args=(4,90,90))
   t3 = threading.Thread(target=controll,args=(3,90,90))
   t4 = threading.Thread(target=controll,args=(5,90,90))
   t5 = threading.Thread(target=controll,args=(6,90,90))
   t6 = threading.Thread(target=controll,args=(8,90,90))
   t1.start()
   t2.start()
   t3.start()
   t4.start()
   t5.start()
   t6.start()
   t1.join()
   t2.join()
   t3.join()
   t4.join()
   t5.join()
   t6.join()
def hi_left():
   t1 = threading.Thread(target=controll,args=(lms,90,160))   
   t1.start()
   t1.join()
   t1 = threading.Thread(target=controll,args=(lsh,90,20,0.04))
   t2 = threading.Thread(target=controll,args=(lel,90,180))
   t3 = threading.Thread(target=speech,args=('self',"hi",1))
   t1.start()
   t2.start()
   t3.start()
   t1.join()
   t2.join()
   t3.join()
   time.sleep(.5)
   t1 = threading.Thread(target=controll,args=(lsh,20,90,0.04))
   
   t2 = threading.Thread(target=controll,args=(lel,180,90))
   t3 = threading.Thread(target=controll,args=(lms,160,90))
   
   t1.start()
   t2.start()
   
   
   t1.join()
   t2.join()
   t3.start()
   t3.join()
def hi_right():
   t1 = threading.Thread(target=controll,args=(rms,90,20))   
   t1.start()
   t1.join()
   t1 = threading.Thread(target=controll,args=(rsh,90,160,0.04))
   t2 = threading.Thread(target=controll,args=(rel,90,0))
   t3 = threading.Thread(target=speech,args=('self',"hi",1))
   t1.start()
   t2.start()
   t3.start()
   t1.join()
   t2.join()
   t3.join()
   time.sleep(.5)
   t1 = threading.Thread(target=controll,args=(rsh,160,90,0.04))
   
   t2 = threading.Thread(target=controll,args=(rel,0,90))
   t3 = threading.Thread(target=controll,args=(rms,20,90))
   
   t1.start()
   t2.start()
   
   
   t1.join()
   t2.join()
   t3.start()
   t3.join()
def left():
   t1 = threading.Thread(target=controll,args=(lms,90,160))   
   t1.start()
   t1.join()
   t1 = threading.Thread(target=controll,args=(lsh,90,20,0.04))
   t2 = threading.Thread(target=controll,args=(lel,90,180))
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   time.sleep(.5)
   t1 = threading.Thread(target=controll,args=(lsh,20,90,0.04))
   
   t2 = threading.Thread(target=controll,args=(lel,180,90))
   t3 = threading.Thread(target=controll,args=(lms,160,90))
   
   t1.start()
   t2.start()
   
   
   t1.join()
   t2.join()
   t3.start()
   t3.join()
def right():
   t1 = threading.Thread(target=controll,args=(rms,90,20))   
   t1.start()
   t1.join()
   t1 = threading.Thread(target=controll,args=(rsh,90,160,0.04))
   t2 = threading.Thread(target=controll,args=(rel,90,0))
   t1.start()
   t2.start()
   
   t1.join()
   t2.join()
   
   time.sleep(.5)
   t1 = threading.Thread(target=controll,args=(rsh,160,90,0.04))
   
   t2 = threading.Thread(target=controll,args=(rel,0,90))
   t3 = threading.Thread(target=controll,args=(rms,20,90))
   
   t1.start()
   t2.start()
   
   
   t1.join()
   t2.join()
   t3.start()
   t3.join()
def one_by_one():
   hi_right()
   hi_left()
def random():
   right()
   left()
def Move():
   t1 = threading.Thread(target=controll,args=(rsh,90,135,0.05))
   t2 = threading.Thread(target=controll,args=(lsh,90,135,0.05))
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   #time.sleep(0.5)
   t1 = threading.Thread(target=controll,args=(rsh,135,90,0.05))
   t2 = threading.Thread(target=controll,args=(lsh,135,90,0.05))  
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   #time.sleep(.5)
   t1 = threading.Thread(target=controll,args=(rsh,90,45,0.05))
   t2 = threading.Thread(target=controll,args=(lsh,90,45,0.05))
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   #time.sleep(0.5)
   t1 = threading.Thread(target=controll,args=(rsh,45,90,0.05))
   t2 = threading.Thread(target=controll,args=(lsh,45,90,0.05))  
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   #time.sleep(.5)
i=1
def RM_front():
   t2 = threading.Thread(target=controll,args=(rms,90,180))
   t2.start()
   t2.join()

   
def both():
 #  t1 = threading.Thread(target=controll,args=(lms,90,0))
 #  t2 = threading.Thread(target=controll,args=(rms,90,180))   
 #  t2.start()
 #  t2.join()
 #  t1.start()
 #  t1.join()
   t2 = threading.Thread(target=controll,args=(rsh,90,140,0.06))
   t1 = threading.Thread(target=controll,args=(rel,90,180))
   #t2 = threading.Thread(target=controll,args=(lel,90,0))
   t1.start()
   t2.start()
   t1.join()
   t2.join()
   time.sleep(0.6)
   t1 = threading.Thread(target=controll,args=(rel,180,90))
   t2 = threading.Thread(target=controll,args=(rsh,140,90,0.06))
   #t2 = threading.Thread(target=controll,args=(lel,0,90))
  # t3 = threading.Thread(target=controll,args=(lms,0,90))
   #t4 = threading.Thread(target=controll,args=(rms,180,90))
   t1.start()
   t2.start()
   t2.join()
   t1.join()
   time.sleep(0.8)
  
   #t3.start()
  # t3.join()
  # t4.start()
  # t4.join()

'''try:
      while 1:
         both()
      procc = subprocess.Popen('ps -ef | grep "/usr/lib/festival/audsp" -c',shell=True, stdout=subprocess.PIPE)
      count1 = int(procc.stdout.read())
      i-=1
      reset()
      print count1
      #reset()
      if (GPIO.input(19)==1):
         while 1:
            print count1
         #random()
            print'random'
            if(count1<=2):
               break
      elif (GPIO.input(19)==1):
         print'hi'
         #Move()
      elif (GPIO.input(17)==1):
         print'both'
         #one_by_one()
      elif (GPIO.input(27)==1):
         print'right_hi'
         hi_right()
      elif (GPIO.input(22)==1):
         print'left_hi'
         hi_left()
      
      
except:
   reset()'''
   

