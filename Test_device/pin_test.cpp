#include <iostream>		// Include all needed libraries here
#include <wiringPi.h>

using namespace std;		// No need to keep using “std”


int main()
{
	wiringPiSetupPhys();

	pinMode(11, INPUT);		// Configure GPI11 as an input

	while(1)
	{

		if(digitalRead(11) == 1) {
			cout << "button on" << endl;
			delay(500); 
		} else {
			cout << "Not on" <<endl;
		}
		delay(500); 
	}

	return 0;
}
